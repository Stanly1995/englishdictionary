﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGeneric
{
    public class MyDictionary<Tkey,TVal> : IDictionary<Tkey,TVal> where Tkey: IComparable
    {
        #region inner classes
        public delegate void ForEachDelegate(KeyValuePair<Tkey, TVal> pair);
        private class DictItem
        {
            public int iD;
            public KeyValuePair<Tkey, TVal> _pair;
            public DictItem _left;
            public DictItem _right;
            public DictItem(Tkey key, TVal value, DictItem left = null, DictItem right = null)
            {
                _pair = new KeyValuePair<Tkey, TVal>(key, value);
                _left = left;
                _right = right;
            }
            public DictItem(KeyValuePair<Tkey, TVal> pair, DictItem left = null, DictItem right = null)
            {
                _pair = pair;
                _left = left;
                _right = right;
            }
        }
        #endregion;

        #region fields
        private DictItem _root = null;
        private int _counter = 0;
        private bool _allowDuplicateKeys;
        #endregion;

        #region ctors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="allowDuplicateKeys">for initialization retry parameter</param>
        public MyDictionary(bool allowDuplicateKeys = false)
        {
            _allowDuplicateKeys = allowDuplicateKeys;
        }
        #endregion;

        #region properties and indexer
        public ICollection<Tkey> Keys => throw new NotImplementedException();

        public ICollection<TVal> Values => throw new NotImplementedException();

        public int Count
        {
            get
            {
                return _counter;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public TVal this[Tkey key]
        {
            get
            {
                return FindVal(key)._pair.Value;
            }
            set
            {

            }
        }

        /// <summary>
        /// this[int iD]
        /// </summary>
        /// <param name="iD">return KeyValuePair for iD</param>
        public KeyValuePair<Tkey, TVal> this[int iD]
        {
            get
            {
                return GetFromId(iD)._pair;
            }
            set
            {
                GetFromId(iD)._pair = value;
            }
        }

        #endregion;

        #region utilites
        /// <summary>
        /// Find Value
        /// </summary>
        /// <param name="key">find KeyValuePair for key</param>
        private DictItem FindVal(Tkey key)
        {
            DictItem find = _root;
            while (true)
            {
                if (find == null)
                    return null;
                if (key.CompareTo(find._pair.Key) == 0)
                {
                    return find;
                }
                else if (key.CompareTo(find._pair.Key) < 0)
                {
                    find = find._left;
                }
                else
                {
                    find = find._right;
                }
            }
        }

        private KeyValuePair<DictItem, DictItem> GetDictPair(Tkey key)
        {
            DictItem item = _root;
            Stack<DictItem> itemStack = new Stack<DictItem>();
            DictItem parent;
            while (item != null || itemStack.Count != 0)
            {
                if (itemStack.Count != 0)
                {
                    item = itemStack.Pop();
                    parent = itemStack.Peek();
                    KeyValuePair<DictItem, DictItem> pair = new KeyValuePair<DictItem, DictItem>(item, parent);
                    if (item._pair.Key.CompareTo(key) == 0)
                        return pair;
                    else
                    {
                        if (item._right != null)
                        {
                            item = item._right;
                        }
                        else
                        {
                            item = null;
                        }
                    }
                }
                while (item != null)
                {
                    itemStack.Push(item);
                    item = item._left;
                }
            }
            return new KeyValuePair<DictItem, DictItem>(null, null);
        }

        private DictItem GetFromId(int iD)
        {
            if (iD < 0 || iD >= _counter)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                DictItem item = _root;
                Stack<DictItem> itemStack = new Stack<DictItem>();
                while (item != null || itemStack.Count != 0)
                {
                    if (itemStack.Count != 0)
                    {
                        item = itemStack.Pop();
                        if (item.iD.CompareTo(iD) == 0)
                            return item;
                        else
                        {
                            if (item._right != null)
                            {
                                item = item._right;
                            }
                            else
                            {
                                item = null;
                            }
                        }
                    }
                    while (item != null)
                    {
                        itemStack.Push(item);
                        item = item._left;
                    }
                }
            }
                return null;
        }
        #endregion;

        #region methods
        public bool ContainsKey(Tkey key)
        {
            DictItem find = _root;
            while (true)
            {
                if (find == null)
                    return false;
                if (key.CompareTo(find._pair.Key) == 0)
                {
                    return true;
                }
                else if (key.CompareTo(find._pair.Key) < 0)
                {
                    find = find._left;
                }
                else
                {
                    find = find._right;
                }
            }
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="key, value">Add KeyValuePair</param>
        public void Add(Tkey key, TVal value)
        {
            AddRecursionLess(new KeyValuePair<Tkey, TVal>(key, value), _root);
        }

        private void AddRecursionLess(KeyValuePair<Tkey, TVal> pair, DictItem item)
        {
            if (item == null)
            {
                DictItem newItem = new DictItem(pair);
                _root = newItem;
                _root.iD = 0;
                ++_counter;
                return;
            }
            else
            {
                while (true)
                {
                    if (!_allowDuplicateKeys && pair.Key.CompareTo(item._pair.Key) == 0)
                    {
                        item._pair = pair;
                        return;
                    }
                    else if (pair.Key.CompareTo(item._pair.Key) < 0)
                    {
                        if (item._left == null)
                        {
                            item._left = new DictItem(pair);
                            item._left.iD = _counter;
                            ++_counter;
                            return;
                        }
                        else
                        {
                            item = item._left;
                        }
                    }
                    else
                    {
                        if (item._right == null)
                        {
                            item._right = new DictItem(pair);
                            item._right.iD = _counter;
                            ++_counter;
                            return;
                        }
                        else
                        {
                            item = item._right;
                        }
                    }
                }
            }
        }

        public void ForEach(ForEachDelegate d)
        {
            if (_root != null)
            {
                ForEach(d, _root);
            }
        }
        private void ForEach(ForEachDelegate d, DictItem item)
        {
            if (item._left != null)
            {
                ForEach(d, item._left);
            }

            d.Invoke(item._pair);

            if (item._right != null)
            {
                ForEach(d, item._right);
            }
        }

        /// <summary>
        /// this[int iD]
        /// </summary>
        /// <param name="key">remove item for key</param>
        public bool Remove(Tkey key)
        {
            if (GetDictPair(key).Key != null)
            {
                if (GetDictPair(key).Key._left == null && GetDictPair(key).Key._right == null)
                {
                    RemoveItemWihtoutChildren(GetDictPair(key).Key, GetDictPair(key).Value);
                }
                else if (GetDictPair(key).Key._left != null && GetDictPair(key).Key._right != null)
                {
                    RemoveItemWihtBothChildren(GetDictPair(key).Key, GetDictPair(key).Value);
                }
                else
                {
                    RemoveItemWihtOneChild(GetDictPair(key).Key, GetDictPair(key).Value);
                }
                --_counter;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void RemoveItemWihtoutChildren(DictItem item, DictItem parent)
        {
            if (item == _root)
            {
                _root = null;
                return;
            }
            if (parent._left == item)
            {
                parent._left = null;
            }
            else
            {
                parent._right = null;
            }
        }

        private void RemoveItemWihtOneChild(DictItem item, DictItem parent)
        {
            if (item == _root)
            {
                if (item._left != null)
                {
                    _root = item._left;
                    return;
                }
                else if (item._right != null)
                {
                    _root = item._right;
                    return;
                }
            }
            else
            {
                if (parent._left == item)
                {
                    if (item._right != null)
                    {
                        parent._left = item._right;
                    }
                    else if (item._left != null)
                    {
                        parent._left = item._left;
                    }
                }
                else
                {
                    if (item._right != null)
                    {
                        parent._right = item._right;
                    }
                    else if (item._left != null)
                    {
                        parent._right = item._left;
                    }
                }
            }

        }

        private void RemoveItemWihtBothChildren(DictItem item, DictItem parent)
        {
            DictItem sucsessor = item._right;
            DictItem sucsessorParent = item;
            {
                while (sucsessor._left != null)
                {
                    sucsessorParent = sucsessor;
                    sucsessor = sucsessor._left;
                }
            }
            item._pair = sucsessor._pair;
            sucsessorParent._left = sucsessor._right;
        }

        public bool TryGetValue(Tkey key, out TVal value)
        {
            value = FindVal(key)._pair.Value;
            if (value != null)
                return true;
            else
                return false;

        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="item">Add KeyValuePair</param>
        public void Add(KeyValuePair<Tkey, TVal> item)
        {
            AddRecursionLess(item, _root);
        }

        public void Clear()
        {
            _counter = 0;
        }

        public bool Contains(KeyValuePair<Tkey, TVal> item)
        {
            if (FindVal(item.Key)._pair.Value.Equals(item.Value))
                return true;
            else
                return false;
        }

        public void CopyTo(KeyValuePair<Tkey, TVal>[] array, int arrayIndex)
        {
            for (int i=0; i<_counter; i++)
            {
                array[arrayIndex + i] = this[i];
            }
        }

        public bool Remove(KeyValuePair<Tkey, TVal> item)
        {
            if (GetDictPair(item.Key).Key != null)
            {
                if (GetDictPair(item.Key).Key._left == null && GetDictPair(item.Key).Key._right == null)
                {
                    RemoveItemWihtoutChildren(GetDictPair(item.Key).Key, GetDictPair(item.Key).Value);
                }
                else if (GetDictPair(item.Key).Key._left != null && GetDictPair(item.Key).Key._right != null)
                {
                    RemoveItemWihtBothChildren(GetDictPair(item.Key).Key, GetDictPair(item.Key).Value);
                }
                else
                {
                    RemoveItemWihtOneChild(GetDictPair(item.Key).Key, GetDictPair(item.Key).Value);
                }
                --_counter;
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion;

        #region IEnumerator

        public IEnumerator<KeyValuePair<Tkey, TVal>> GetEnumerator()
        {
            using (IEnumerator<DictItem> e = GetDictItemEnumerator(_root))
            {
                while (e.MoveNext())
                {
                    yield return e.Current._pair;
                }
            }
        }

        private IEnumerator<DictItem> GetDictItemEnumerator(DictItem item)
        {
            Stack<DictItem> itemStack = new Stack<DictItem>();
            while (item != null || itemStack.Count != 0)
            {
                if (itemStack.Count != 0)
                {
                    item = itemStack.Pop();
                    yield return item;
                    if (item._right != null)
                    {
                        item = item._right;
                    }
                    else
                    {
                        item = null;
                    }
                }
                while (item != null)
                {
                    itemStack.Push(item);
                    item = item._left;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            using (IEnumerator<DictItem> e = GetDictItemEnumerator(_root))
            {
                while (e.MoveNext())
                {
                    yield return e.Current._pair;
                }
            }
        }

        #endregion;
    }
}
