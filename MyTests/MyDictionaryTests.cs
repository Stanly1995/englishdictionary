﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyGeneric;

namespace MyTests
{
    [TestClass]
    public class MyDictionaryTests
    {

        [TestMethod]
        public void MyDictionaryCtorTest()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            Assert.AreNotEqual(null, dictionary);
            Assert.AreEqual(0, dictionary.Count);
            Assert.AreEqual(false, dictionary.IsReadOnly);
        }

        [TestMethod]
        public void CounterTestMethod()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            dictionary.Add("1", "4");
            dictionary.Add("2", "3");
            dictionary.Add("3", "2");
            dictionary.Add("4", "1");
            Assert.AreEqual(4, dictionary.Count);
        }

        [TestMethod]
        public void MyDictionaryClearTest()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            dictionary.Add("1", "4");
            dictionary.Add("2", "3");
            dictionary.Add("3", "2");
            dictionary.Add("4", "1");
            Assert.AreEqual(4, dictionary.Count);
            dictionary.Clear();
            Assert.AreEqual(0, dictionary.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MyDictionaryOutOfRangeTest_0()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            dictionary[-1] = new System.Collections.Generic.KeyValuePair<string, string>();
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MyDictionaryOutOfRangeTest_1()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            dictionary[1] = new System.Collections.Generic.KeyValuePair<string, string>();
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MyDictionaryOutOfRangeTest_2()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            System.Collections.Generic.KeyValuePair<string, string> x = dictionary[-1];
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MyDictionaryOutOfRangeTest_3()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            System.Collections.Generic.KeyValuePair<string, string> x = dictionary[1];
        }

        [TestMethod]
        public void MyDictionaryContainsTest()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            dictionary.Add("1", "4");
            dictionary.Add("2", "3");
            dictionary.Add("3", "2");
            dictionary.Add("4", "1");
            Assert.IsFalse(dictionary.Contains(new System.Collections.Generic.KeyValuePair<string, string>("1", "2")));
            Assert.IsTrue(dictionary.Contains(new System.Collections.Generic.KeyValuePair<string, string>("1", "4")));
            Assert.IsFalse(dictionary.Contains(new System.Collections.Generic.KeyValuePair<string, string>("2", "2")));
            Assert.IsTrue(dictionary.Contains(new System.Collections.Generic.KeyValuePair<string, string>("2", "3")));
            Assert.IsTrue(dictionary.Contains(new System.Collections.Generic.KeyValuePair<string, string>("3", "2")));
            Assert.IsTrue(dictionary.Contains(new System.Collections.Generic.KeyValuePair<string, string>("4", "1")));
        }

        [TestMethod]
        public void MyDictionaryIndexOfTest()
        {
            MyDictionary<string, string> dictionary = new MyDictionary<string, string>();
            dictionary.Add("1", "4");
            dictionary.Add("2", "3");
            dictionary.Add("3", "2");
            dictionary.Add("4", "1");
            Assert.AreEqual("4", dictionary["1"]);
            Assert.AreEqual("3", dictionary["2"]);
            Assert.AreEqual("2", dictionary["3"]);
            Assert.AreEqual("1", dictionary["4"]);
        }
    }
}
