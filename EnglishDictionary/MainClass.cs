﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGeneric;
using System.IO;

namespace EnglishDictionary
{
    public class MainClass
    {
        static void Test(KeyValuePair<string, string> pair)
        {
            Console.WriteLine($"[{pair.Key}] = {pair.Value}");
        }
        static void Main(string[] args)
        {
            Console.Clear();
            MyDictionary<string, string> eng = ActionsWithDictionary.WriteToMyDictionary();
            while (true)
            {
                Console.WriteLine($"Напишите: <add> - для добавления слов, <test> - для самопроверки, или <exit> - для выхода");
                string action = Console.ReadLine();
                if (action.Equals("test"))
                    ActionsWithDictionary.TestWithMyDictionary(eng);
                else if (action.Equals("exit"))
                    break;
                else if (action.Equals("add"))
                    ActionsWithDictionary.AddToMyDictionary(eng);
            }

        }
    }
}
