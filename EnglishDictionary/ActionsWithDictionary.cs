﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyGeneric;
using System.IO;

namespace EnglishDictionary
{
    public static class ActionsWithDictionary
    {
        public static MyDictionary<string, string> WriteToMyDictionary()
        {
            MyDictionary<string, string> eng = new MyDictionary<string, string>();
            using (StreamReader reader = new StreamReader(File.Open
                ($"..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}eng-rus.txt",
                FileMode.OpenOrCreate, FileAccess.Read), Encoding.UTF8))
            {
                string word = reader.ReadLine();
                while (word != null)
                {
                    eng.Add(word.Split('-')[0], word.Split('-')[1]);
                    word = reader.ReadLine();
                }
            }
            return eng;
        }

        public static void TestWithMyDictionary(MyDictionary<string, string> myDictionary)
        {
            while (true)
            {
                Console.Clear();
                Random rnd = new Random();
                int rand = rnd.Next(myDictionary.Count);
                string dictWord = myDictionary[rand].Value;
                Console.WriteLine("Для выхода в главное меню, напишите <to main>");
                Console.WriteLine("Напишите перевод слова: " + dictWord);
                int mistake = 0;
                    while (true)
                    {
                    string myWord = Console.ReadLine();
                    if (myWord == "to main")
                    {
                        Console.Clear();
                        return;
                    }
                    else
                    {
                        try
                        {
                            if (myDictionary[myWord].Equals(dictWord))
                            {
                                Console.WriteLine("Верно");
                                mistake = 0;
                                Console.ReadKey();
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Неверно");
                                mistake++;
                            }
                        }
                        catch (NullReferenceException)
                        {
                            Console.WriteLine("Неверно");
                            mistake++;
                        }

                        if (mistake == 3)
                        {
                            Console.WriteLine("Правильное слово: " + myDictionary[rand].Key);
                            mistake = 0;
                            Console.ReadKey();
                            break;
                        }
                    }
                }
            }
        }

        public static void AddToMyDictionary(MyDictionary<string, string> myDictionary)
        {
            Console.Clear();
            Console.WriteLine("Для добавления слов впишите <word><-><перевод>. Для выхода в главное меню - напишите <to main>");
            Console.WriteLine("Слово-Перевод");
            for (int i = 0; i < myDictionary.Count; i++)
            {
                Console.WriteLine(myDictionary[i].Key + "-" + myDictionary[i].Value);
            }

            while (true)
            {
                string newWord = Console.ReadLine();
                if (newWord.Split('-')[0] == "to main")
                {
                    Console.Clear();
                    return;
                }
                else if (newWord.Split('-')[0] != "" && newWord.Split('-')[1] != "")
                {
                    using (StreamWriter writer = new StreamWriter(File.Open
                    ($"..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}eng-rus.txt",
                    FileMode.Append, FileAccess.Write), Encoding.UTF8))
                    {
                        Console.Clear();
                        Console.WriteLine("Для добавления слов впишите <word><-><перевод>. Для выхода в главное меню - напишите <to main>");
                        writer.WriteLine(newWord.Split('-')[0] + "-" + newWord.Split('-')[1]);
                        myDictionary.Add(newWord.Split('-')[0], newWord.Split('-')[1]);
                        Console.WriteLine("Слово - Перевод");
                        for (int i = 0; i < myDictionary.Count; i++)
                        {
                            Console.WriteLine(myDictionary[i].Key + "-" + myDictionary[i].Value);
                        }
                    }

                }
            }
        }
    }
}
